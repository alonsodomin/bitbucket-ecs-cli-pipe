# Bitbucket Pipelines Pipe: ecs-cli

A BitBucket Pipe to deploy ECS services using the `ecs-cli` tool

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: alonsodomin/bitbucket-ecs-cli-pipe:latest
  variables:
    AWS_ACCESS_KEY_ID:
    AWS_SECRET_ACCESS_KEY:
    AWS_DEFAULT_REGION:
    ECS_CLUSTER_NAME:
    ECS_LAUNCH_TYPE: # Optional
    PROJECT_NAME:
```

## Variables

<pipe_variables_table>

## Details

<pipe_long_description>

## Prerequisites

<pipe_prerequisites>

## Examples

<pipe_code_examples>

## Support

<pipe_support>