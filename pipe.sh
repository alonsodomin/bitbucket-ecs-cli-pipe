#!/usr/bin/env bash

set -e

echo "Configuring the ECS CLI"
ecs-cli configure profile --access-key $AWS_ACCESS_KEY_ID --secret-key $AWS_SECRET_ACCESS_KEY
ecs-cli configure --cluster $ECS_CLUSTER_NAME --default-launch-type $ECS_LAUNCH_TYPE --region $AWS_DEFAULT_REGION --config-name $ECS_CLUSTER_NAME

echo "Deploying service"
ecs-cli compose --project-name $PROJECT_NAME service up --create-log-groups --cluster-config $ECS_CLUSTER_NAME